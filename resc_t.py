import abc


class DataRessource(abc.ABC):
    address = None;

    @abc.abstractmethod
    def open(self):
        """Function which should be implemented to return an instance
        to the data ressource, like a file (form disk or url), or like
        a connection to database, etc..."""
        raise NotImplementedError;

    @abc.abstractmethod
    def close(self):
        """Function which should be implemented to close the 
        ressource."""
        raise NotImplementedError;


class TextRessource(DataRessource):
    def __init__(self, target):
        """Constructor of a text data ressource"""
        self.__text = target;

    def open(self):
        """Function which should be implemented to return an instance
        to the python string type"""
        return self.__text;

    def close(self):
        """Function of closing ressource"""
        return False;

pass;
