"""
    Monday October 04th, 2021 by Dr Mokira
"""
import os
import queue
import concurrent.futures as cf
import time
import logging
import random

from multiprocessing import Process


# Configuration of logger for state displaying
logger_format = "%(asctime)s\t%(threadName)s\t%(message)s";
logging.basicConfig(format=logger_format, level=logging.INFO, datefmt="%H:%M:%S");


class Processor(Process):
    # Class variables
    REPLAY = -1;

    @property
    def isrunning(self):
        return self.__is_running;

    def __init__(self, cpuc=None):
        """ Constructor of a processor """
        super(Processor, self).__init__();

        # variables of processor
        self.__is_running = False;
        if cpuc is None:
            self.__cpu_count = os.cpu_count() if cpuc is None
        else:
            self.__cpu_count = cpuc;

        self.__end_task_callback = lambda x: x;
        self.__end_callback      = lambda y: y;
        self.__tasks = queue.Queue();

    # def stop(self):
    #     """ Function that stops transaction process """
    #     self.__is_running = False;

    def set_end_task_cb(self, cb):
        """ Function of end task callback setting """
        self.__end_task_callback = cb;

    def set_end_cb(self, cb):
        """ Function of en callback setting """
        self.__end_callback = cb;

    def put_task(self, task):
        """ Function that is used to add a new task """
        if task is not None:
            self.__tasks.put(task);
            return task;
        else:
            return None;

    def start(self):
        """ Redifining of start function """
        self.__is_running = True;
        return super().start();

    def _on_start(self):
        """ Function that execute during program starting """
        logging.info("On starting ...");
        return;

    def run(self):
        """ Program of execution of tasks """
        self._on_start();

        while not self.__tasks.empty() and self.__is_running:
            with cf.ThreadPoolExecutor(max_workers=self.__cpu_count) as executor:
                # future_to_mapping = {
    			#         executor.submit(mainf, i, num_word_mapping[i]): num_word_mapping[i] for i in range(1, 10)
    		    # };
                future_map = {};
                index = 0;

                while not self.__tasks.empty():
                    # we get a new task
                    # we submit this task in thread pool executor
                    task = self.__tasks.get();
                    future_map[executor.submit(self.mainf, task)] = index;
                    index += 1;

                # we waitting each end of task
                # we recovery the integer reterned by task
                # if task has REPAY status, then we put again in queue
                # if task has different status of preview cas, then call the callback function
                for future in cf.as_completed(future_map):
                    result = future.result();

                    if type(result) is tuple:
                        if result[1] == Processor.REPLAY:
                            self.put_task(result[0]);
                        else:
                            self.__end_task_callback(result[0]);

                    else:
                        raise ValueError("You must to return a tuple witch contain \
                        the task and an integer as status.");

        self.__is_running = False;
        self.__end_callback(self.__tasks.qsize());
        return None;

    def mainf(self, task):
        """ Function to execute one task """
        t = random.randint(1, 20);
        logging.info(f"{task} is started for {t} sec");
        time.sleep(t);
        logging.info(f"{task} is done in {t} sec !");
        return task, (0 if t % 2 == 0 else -1);

pass;
