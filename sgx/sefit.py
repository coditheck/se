import sys
import spacy

import sgx.idx_ptn as idxp
import sgx.pcsys as pcsys
import sgx.ios   as ios
import sgx.se    as se


# Logging configuration
logi = ios.InfoLog().on(True);
# NLP  = spacy.load('fr_core_news_md');


def get_tokens(doc, filter_f=lambda x: True):
    """ Function of NLP doc tokens filter """
    for token in doc:
        if filter_f(token):
            yield token;


class TokenCounter(se.SEProc):
    def init_f(self, state):
        # assert len(state.txtdocs) > 0, ("Doc text missing ...");
        pass;

    def proc_f(self, state, data):
        """ I remove the punctuations and the stop words and count number of occurences 
            of all the token."""
        # def check_token_lv1(k):
        #    # Function which checks if the token 'k' not 
        #    # punctuation and stop word
        #    return (not k.is_punct\
        #                and not k.is_stop\
        #                and '\n' not in k.text \
        #                # and ' ' not in k.text \
        #                and not k.pos_ == 'SPACE'\
        #                and not k.pos_ == 'VERB'\
        #                and not k.pos_ == 'ADJ'\
        #                and not k.pos_ == 'ADV'\
        #    );

        def reg_counts(elem, dict_):
            # Function which update the counts
            if elem in dict_: dict_[elem] += 1;
            else:
                dict_[elem] = 1;

            # return updated counts
            return dict_;

        # create an instance spacy matcher and add the differents patterns
        matcher = spacy.matcher.Matcher(se.NLP.vocab);
        matcher.add('mmat_01', idxp.pattern);

        state.counts = {};
        for txtf in state.txtdocs:
            txtfile = se.TextFileRess(txtf);
            if txtfile.open():
                text = txtfile.read();
                txtfile.close();
                doc = se.NLP(text);
                matches = matcher(doc);
                # for token in tokens: print(token, token.pos_);
            else:
                continue;

            # I count the token lemma
            # docl = len(doc);
            for match_id, start, end in matches:
                span = doc[start:end];
                kws  = '';

                if len(span) > 1: kws = span.text;
                else:
                    dock = se.NLP(span.text);
                    kws  = dock[0].lemma_;

                state.counts = reg_counts(kws.lower(), state.counts);

        # I sorte the dict before to return the state
        state.counts = dict(sorted(state.counts.items(), key=lambda item: item[1], reverse=True));
        return state;


class KwExtraction(se.SEProc):
    def init_f(self, state):
        coun = list(state.counts.values());
        if len(coun) == 0:
            raise pcsys.StopProcessing();

        min_ = coun[-1];
        max_ = coun[0];
        stdd = max_ - min_;           # print(stdd);
        avr  = stdd / state.SBT_AVR;  # print(avr);

        state.avr = avr;
        return state;

    def proc_f(self, state, data):
        keywords = [];
        for token, count in state.counts.items():
            if count >= state.avr:
                keywords.append((token, count));
            else:
                break;

        state.keywords = keywords;
        return state;


# class KwMixer(se.SEProc):
#    def init_f(self, state):
#        pass;
#
#    def proc_f(self, state, data):
#        """ Process of keyword mixing with doc """
        


class ModelUpdating(se.SEProc):
    def init_f(self, state):
        file = se.TextFileRess(state.mfile);
        codc = se.ModelCodec();
        model = se.SEModel();
        state.model = model;

        if file.check():
            file.open();
            data = file.read();
            data = codc.decode(data);
            state.model.fetch(data);

    def proc_f(self, state, data):
        state.model.update(state.keywords, state.docref);
        file = se.TextFileRess(state.mfile);
        codc = se.ModelCodec();

        if file.check(): file.open(mode='w');
        elif file.open('x'):
            pass;
        else:
            sys.exit(1);

        data = codc.encode(state.model);
        file.write(data);
        file.close();


if __name__ == '__main__':
    # I create the text file ressources
    # logi("Initialization of ressources ...");
    # ress = [
    #    TextFileRess('ress/file0.txt'),
    #    TextFileRess('ress/file1.txt'),
    #    TextFileRess('ress/file2.txt'),
    #    TextFileRess('ress/file3.txt'),
    #    TextFileRess('ress/file4.txt'),
    #    TextFileRess('ress/file5.txt'),
    #    TextFileRess('ress/file6.txt'),
    #    TextFileRess('ress/file7.txt'),
    #    TextFileRess('ress/file8.txt'),
    #    TextFileRess('ress/file9.txt'),
    # ];

    # logi("Openning of ressources ...");
    # for res in ress:
    #    res.open();

    # for res in ress:
    #    print(res.read());
    #    input("[ENTER]");

    keyword_extraction = KwExtraction();
    model_updating     = ModelUpdating();
    token_counter      = TokenCounter();

    indexation_proc = se.SEProcSeq();
    indexation_proc.add_proc(token_counter);
    indexation_proc.add_proc(keyword_extraction);
    indexation_proc.add_proc(model_updating);

    class St(object): pass;

    state = St();
    state.filedoc = 'ress/file9.txt';   # A new text file
    state.mfile   = 'se.mdl.ia';        # Model file

    kernel = pcsys.Kernel();
    p, q = kernel.start_proc(indexation_proc, state);
    res  = kernel.wait_result(q);

    print();
    print(res.keywords);
    
