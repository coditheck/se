import importlib
import inspect


def importcls(modulestr, subclsof=[object]):
    ## Function that is used to import class and check if this class
    ## is a sub class of the class specified at `subclsof` parameter
    assert type(modulestr) is str, ("`modulestr` must be a string.");
    mdulname, clsname = modulestr.rsplit(".", 1);
    mdul   = importlib.import_module(mdulname);
    class_ = getattr(mdul, clsname);

    assert inspect.isclass(class_), (f"{modulestr} is not a class.");
    cond = False;
    for sc in subclsof:
        cond = cond or issubclass(class_, sc);

    assert cond, (f"{modulestr} is not a sub class of no {subclsof}");
    return class_;


def importobj(modulestr, instof=[object]):
    ## Function that is used to import class and check if this class
    ## is a sub class of the class specified at `subclsof` parameter
    assert type(modulestr) is str, ("`modulestr` must be a string.");
    mdulname, instname = modulestr.rsplit(".", 1);
    mdul = importlib.import_module(mdulname);
    inst = getattr(mdul, instname);
    cond = False;

    for clss in instof:
        cond = cond or isinstance(inst, clss);

    # assert not inspect.isclass(class_), (f"{modulestr} is not a class.");
    assert cond, (f"{modulestr} is not an instance of {instof}");
    return inst;

pass;
