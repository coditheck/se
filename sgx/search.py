import sys
import spacy

import sgx.pcsys as pcsys
import sgx.se    as se


# NLP = spacy.load('fr_core_news_md');
SIMILARITY_MAX = 0.8;


class ModelLoading(se.SEProc):
    def init_f(self, state):
        pass;

    def proc_f(self, state, data):
        file = se.TextFileRess(state.mfile);
        codc = se.ModelCodec();
        model = se.SEModel();
        state.model = model;

        if file.check() and file.open():
            data = file.read();
            file.close();
            data = codc.decode(data);
            state.model.fetch(data);
        else:
            sys.exit(1);


class Search(se.SEMulProc):
    def init_f(self, state):
        if not hasattr(state.model, 'keywords')\
                or type(state.model.keywords) is not list\
                or len(state.model.keywords) == 0\
                or type(state.inquery) is not str\
                or state.inquery == '':
            raise pcsys.StopProcessing();

        # remove punstuations and stop words
        # we make the lemmatization
        tokens = [];
        doc    = se.NLP(state.inquery);

        for token in doc:
            if not token.is_punct and not token.is_stop:
                tokens.append(token.lemma_);

        # cleaning
        tokens = [token\
                for token in tokens\
                if '\n' not in token\
                    and '\xa0' not in token
        ];
        state.query = tokens;
        state.query.append(' '.join(tokens));

        # mult configuration
        self.dset = state.model.keywords;
        self.ndiv = 10;

    def __merge(self, array1, array2):
        for el2 in array2:
            if el2 not in array1:
                array1.append(el2);

        return array1;

    def d_proc_f(self, state, keywords, dx):
        results = [];
        scores  = [];

        for x in dx:
            keyword = keywords[x];
            kw_doc  = se.NLP(keyword);

            for q in state.query:
                query = se.NLP(q);
                score = query.similarity(kw_doc);
                res   = [];

                if score >= state.SIM_MIN:
                    scores.append(score);
                    for idx in state.model.docindex:
                        if idx['key'] == keyword:
                            doc = idx['doc'];
                            if doc not in results:
                                res.append(doc);

                results = self.__merge(results, res);

        return results;

    def proc_f(self, state, data):
        state.results = [];
        for results in data:
            if hasattr(results, '__iter__'):
                state.results = self.__merge(state.results, results);

        return state;


if __name__ == '__main__':
    kernel   = pcsys.Kernel();
    model_ld = ModelLoading();
    search   = Search();

    search_proc = pcsys.ProcSeq();
    search_proc.add_proc(model_ld);
    search_proc.add_proc(search);

    query = '';
    while 1:
        query = input(">_ ");

        if query != 'exit':
            class St(object): pass;

            state = St();
            state.inquery = query;   # A new text file
            state.mfile   = 'se.mdl.ia';        # Model file

            p, q = kernel.start_proc(search_proc, state);
            res  = kernel.wait_result(q);

            print();
            for r in res.results:
                print(r);
        else: break;

        print();
    


pass;
