import abc
import logging


class BaseRess(abc.ABC):
    """This structure represents a data ressource. It's a abstract class which contains to method
    which should implement:
        - open()
        - close()
    """
    address = None;

    @property
    def isopen(self):
        """This property must be implemented to return a bool, when we check if a ressource
        is open, or not. Default, it returns False value."""
        return False;

    @abc.abstractmethod
    def open(self, mode=None):
        """This function will be implemented to open a stream to the ressource"""
        raise NotImplementedError;

    @abc.abstractmethod
    def read(self, **options):
        """This function will be implemented to read data from the open ressource stream"""
        raise NotImplementedError;

    def write(self, data, **options):
        """This function will be implemented to write data to stream ressource"""
        raise NotImplementedError;

    @abc.abstractmethod
    def close(self):
        """This function will be implemented to close the ressouce stream"""
        raise NotImplementedError;


class BaseCodec(abc.ABC):
    """This structure represents the operation to do on the ressource stream instance"""

    @abc.abstractmethod
    def encode(self, data):
        """This function will be implemented to encode data to send to a ressource stream.
        It should return the encoded data."""
        raise NotImplementedError;

    @abc.abstractmethod
    def decode(self, encoded_data):
        """This function will be implemented to decode data and to return it."""
        raise NotImplementedError;


class Log(object):
    DEFAULT_CONF  = {};

    def __init__(self, log_f=None, config=None):
        """Constructor of a log instance"""
        # self._log_f = log_f  if log_f  is not None else self.DEFAULT_LOG_F;
        self._conf  = config if config is not None else self.DEFAULT_CONF;
        self._on    = True;

    def on(self, val):
        """Function used to enable the log system."""
        self._on = val;
        assert type(val) is bool, (
            "The value to use for to ON log must be a bool type."
        );

        # I return the same instance
        # of this log system
        return self;

    def __call__(self, *args, **kwargs):
        """Logging message"""
        if self._on:
            print(*args, **kwargs);


class InfoLog(Log):
    DEFAULT_LOG_F = logging.info;
    DEFAULT_CONF  = {
        'datefmt' : "%I:%M:%S",
        'format'  : "[%(asctime)s %(msecs)03dms] [PID %(process)d] %(message)s",
        'level'   : logging.INFO,
    };

    def __init__(self, log_f=None, config=None):
        """Constructor of log instance"""
        super(InfoLog, self).__init__(log_f, config);

        # Log system initialization
        logging.basicConfig(
            datefmt=self._conf['datefmt'],
            format=self._conf['format'],
            level=self._conf['level']
        );

    def __call__(self, *args, **kwargs):
        """Logging message"""
        if self._on:
            logging.info(*args, **kwargs);


pass;
