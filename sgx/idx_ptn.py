wpat = {
    'IS_PUNCT': False, 
    'IS_SPACE': False,
    'IS_STOP':  False,
    'POS':      {'NOT_IN': [
        'SPACE',
        'VERB',
        'ADJ',
        'ADV',
        'AUX',
    ]},
};
wspat = wpat.copy();
wspat.update({'OP': '+'});

adppat = {'POS': 'ADP'};
adjpat = {'POS': 'ADJ', 'OP': '?'};
detpat = {'POS': 'DET', 'OP': '?'};

pattern1 = [wpat, adppat, detpat, wpat];
pattern2 = [wspat];
pattern0 = [wpat];

pattern = [pattern1, pattern2, pattern0];
pass;
