import os
import json
import time
import random
import spacy

import sgx.stdlib as stdl
import sgx.pcsys  as pcsys
import sgx.ios    as ios


logi = ios.InfoLog().on(True);
NLP  = spacy.load('fr_core_news_md');


class SERess(ios.BaseRess):
    pass;

class SEProc(pcsys.Proc):
    ## This structure represent the basic search engine processing.
    pass;


class SEProcSeq(pcsys.ProcSeq):
    ## This structure represent the basic search engine processing sequence.
    pass;


class SEMulProc(pcsys.MulProc):
    ## Definition of basic search engine of multi-processing.
    pass;


class TextFileRess(SERess):
    def __init__(self, path):
        """Constructor of a text file ressource"""
        self.__fstream = None;
        self.address   = path;

    @property
    def isopen(self):
        return self.__fstream is not None;

    def check(self):
        """Function that is used to check if ressource is exists"""
        logi(f"Checking ressource at {self.address} ... ");
        return os.path.exists(self.address);

    def open(self, mode='r'):
        """Function of file openning"""
        assert type(self.address) is str, (
            "The path of file must be a string."
        );
        try:
            instance = open(self.address, mode, encoding='utf-8');
            self.__fstream = instance;

            logi(f"Text file is open in [{mode}] mode from {self.address} !");
            return self.__fstream;
        except:
            logi("[ERROR] Unable to open this file.");
            return False;

    def read(self, **options):
        """Function of reading file"""
        logi(f"Text reading from this {self.address} ... ");
        if self.isopen:
            try:
                text = self.__fstream.read();
                logi(f"Text reading done !");
                return text;
            except:
                print("[ERROR] Unable to read the text from this file.");
                return False;
        else:
            print("[ERROR] This text file ressource is not open.");
            return False;

    def write(self, data):
        """ Function of the file writing."""
        logi(f"Text writing to {self.address} ... ");
        if self.isopen:
            try:
                self.__fstream.write(data);
                logi(f"Text writing to {self.address} ... DONE !");
                return True;
            except:
                print("[ERROR] Unable to read the text from this file.");
                return False;
        else:
            print("[ERROR] This text file ressource is not open.");
            return False; 

    def close(self):
        """Closing ressource function"""
        self.__fstream.close();
        logi(f"File of {self.address} is closed.");
        return True;


class SEModel(object):
    MDL_FILE = 'se.mdl.ia';

    def __init__(self):
        super(SEModel, self).__init__();
        self.__keyworks = [];
        self.__doc_idxs = [];
        self.__docs     = [];

    @property
    def keywords(self):
        return self.__keyworks;

    @property
    def docindex(self):
        return self.__doc_idxs;

    @property
    def docs(self):
        return self.__docs;

    def fetch(self, data):
        logi("Fetching model data ...");
        assert type(data) is dict, (
            "data must be a dict."
        );
        self.__keyworks = data.get('__keyworks', []);
        self.__doc_idxs = data.get('__doc_idxs', []);
        self.__docs     = data.get('__docs', []);

        if self.__keyworks is None: self.__keyworks = [];
        if self.__doc_idxs is None: self.__doc_idxs = [];
        if self.__docs     is None: self.__docs     = [];

        logi("Fetching model data ... DONE !");
        return self;

    def update(self, keywords, doc):
        logi("Model data updating ...");
        if doc not in self.__docs:
            self.__docs.append(doc);

        for kw in keywords:
            if kw not in self.__keyworks:
                self.__keyworks.append(kw);

            self.__doc_idxs.append({
                'score': kw[1],
                'key': kw[0],
                'doc': doc,
            });

        logi("Model data updating ... DONE !");
        return True;


class ModelCodec(ios.BaseCodec):
    def encode(self, model):
        """This function will be implemented to encode data to send to a ressource stream.
        It should return the encoded data."""
        logi("Coding model data ...");
        assert isinstance(model, SEModel), (
            "model must be a SEModel instance."
        );
        data = {
            'didx': model.docindex,
            'docs': model.docs,
            'kws' : model.keywords,
        };

        logi("Coding model data ... DONE !");
        return json.dumps(data);

    def decode(self, encoded_data):
        """This function will be implemented to decode data and to return it."""
        logi("Decoding model data ...");
        data = json.loads(encoded_data);

        logi("Decoding model data ... DONE !");
        return {
            '__keyworks':  data.get('kws'),
            '__doc_idxs':  data.get('didx'),
            '__docs': data.get('docs'),
        };



class St(object): pass;


class SE(object):
    # search engin model
    MODEL_CLS = None;

    # STREAM CONFIG
    MDL_CDC = None;
    TXT_RES = None;

    # MATCHING CAPACITY
    SIM_MIN = None;
    SBT_AVR = None;

    # indexation processong
    IDEX_PROCS = [];

    # search processing
    SRCH_PROCS = [];

    def __init__(self):
        ## Constructor of an search engin
        super(SE, self).__init__();
        self.__kernel = pcsys.Kernel();

    def fit(self, txtdocs, doc=None):
        """ Indexation process of this search engine """
        if doc is None:
            doc = random.randint(0, round(time.time()));

        idxproc = SEProcSeq();
        for procm in SE.IDEX_PROCS:
            proccls = stdl.importcls(procm, [SEProc, SEMulProc]);
            idxproc.add_proc(proccls());

        # state configuration
        state = St();
        state.txtdocs = txtdocs;
        state.docref  = doc;
        state.SBT_AVR = SE.SBT_AVR;
        state.mfile   = SEModel.MDL_FILE;

        # starting process seq
        # p, q = self.__kernel.start_proc(idxproc, state);
        # res  = self.__kernel.wait_result(q);
        res = self.__kernel.exec_proc(idxproc, state);
        res = res[0];
        return res.keywords if hasattr(res, 'keywords') else [];

    def search(self, query):
        ## Researching function
        seaproc = SEProcSeq();
        for procm in SE.SRCH_PROCS:
            proccls = stdl.importcls(procm, [SEProc, SEMulProc]);
            seaproc.add_proc(proccls());

        # state configuration
        state = St();
        state.inquery = query;
        state.SIM_MIN = SE.SIM_MIN;
        state.mfile   = SEModel.MDL_FILE;

        # starting process seq
        # p, q = self.__kernel.start_proc(seaproc, state);
        # res  = self.__kernel.wait_result(q);
        res = self.__kernel.exec_proc(seaproc, state);
        res = res[0];
        return res.results if hasattr(res, 'results') else [];

pass;
