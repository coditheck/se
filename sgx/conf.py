from .se import SE


# search engin model
SE.MODEL_CLS = 'sgx.se.SEModel';

# STREAM CONFIG
SE.MDL_CDC = 'sgx.se.ModelCodec';
SE.TXT_RES = 'sgx.se.TextFileRess';

# MATCHING CAPACITY
SE.SIM_MIN = 0.7;
SE.SBT_AVR = 5.0;

# indexation processong
SE.IDEX_PROCS = [
    'sgx.sefit.TokenCounter',
    'sgx.sefit.KwExtraction',
    'sgx.sefit.ModelUpdating',
];

# search processing
SE.SRCH_PROCS = [
    'sgx.search.ModelLoading',
    'sgx.search.Search',
];

