from resc_t import DataRessource


class BaseDataExtractor(object):
    @property
    def data(self):
        """Texts list property"""
        return self._data;

    def __init__(self):
        """ Constructor of text extractor from ressorces """
        super(BaseDataExtractor, self).__init__();
        self._ress = [];
        self._data = [];

    def add_res(self, res):
        """Function that is used to add ressources"""
        if type(res) is list:
            for r in res:
                self.add_res(r);
        else:
            assert isinstance(res, DataRessource), (
                "The ressource number {} must be a DataRessouce type."
            );
            self._ress.append(res);

        # we return that
        return res;

    def extraction_f(self, ressource):
        """Function which contains the extraction implementation 
        of a pice of data. This function must be implemented by the
        programmer and should return a data."""
        raise NotImplementedError;

    def start_extraction(self):
        for res in self._ress:
            data = self.extraction_f(res);
            self._data.append(data);


class TextExtractor(BaseDataExtractor):
    @property
    def texts(self):
        """Texts list property"""
        return self.data;

    def __init__(self):
        """Constructor of text extractor from ressorces"""
        super(TextExtractor, self).__init__();

    def extraction_f(self, ressource):
        """Function of text data extraction from differents ressources"""
        instance = ressource.get_instance();
        if type(instance) is str:
            return instance;
        else:
            return "";

pass;
