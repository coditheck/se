import pcsys
import ioc


class Object(object):
    def __init__(self):
        self.inivalue = None;
        self.value    = None;

    def __str__(self):
        return "inivalue = {}\t\tvalue = {}".format(
            self.inivalue,  
            self.value
        );


class FirstProc(pcsys.Proc):
    def init_f(self, state: object):
        state.inivalue = "FG";
        print(state);
        return state;

    def proc_f(self, state: object):
        state.inivalue = 0;
        state.value    = range(20);
        print(state);
        return state;


class SecondProc(pcsys.Proc):
    def init_f(self, state: object):
        state.inivalue += 1000;
        print(state);
        return state;

    def proc_f(self, state: object):
        state.inivalue += 1000;
        state.value = range(100);
        print(state);
        return state;


if __name__ == '__main__':
    kernel = pcsys.Kernel();
    p1 = FirstProc();
    p2 = SecondProc();

    proc_seq1 = pcsys.ProcSeq();
    proc_seq2 = pcsys.ProcSeq();
    proc_seq1.add_proc(p1);
    proc_seq1.add_proc(p2);

    proc_seq2.add_proc(p2);
    proc_seq2.add_proc(p1);
    proc_seq1.add_proc(proc_seq2);

    state = Object();
    p, q  = kernel.start_proc(proc_seq1, state);
    res = kernel.wait_result(q);

    print();
    print(res);


pass
