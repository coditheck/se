import time   as tm
import pandas as pd
import spacy

from tqdm import tqdm
from rank_bm25 import BM25Okapi


class Sengy(object):
    nlp = spacy.load('fr_core_news_sm', disable=['ner', 'tagger', 'parser']);
    # nlp = spacy.load('fr_core_news_sm');

    def __init__(self):
        """ Constructor of Sengy search engine """
        super(Sengy, self).__init__();
        self.__indexsm = [];
        self.__sources = [];

    def index_data(self, data):
        """Function of indexing text data for the search engin"""
        ## text extrationg
        if type(data) is not list:
            data = [data];

        texts = [text.lower() for text in data];
        self.__sources.append(data);

        ## text tokenization
        token_vecs = [];
        for i, text in enumerate(texts):
            token_vecs.append([]);
            for token in tqdm(self.nlp(text)):
            # for doc in self.nlp(texts):
                # tokens = [
                #    t.text\
                #    for t in doc\
                #    if t.is_alpha or t.is_num\
                # ];
                if token.is_alpha or token.is_digit:
                    token_vecs[i].append(token.text);
                    # print(token);

        if len(token_vecs) > 0:
            bm25 = BM25Okapi(token_vecs);
            self.__indexsm.append(bm25);
            return bm25;

    def search(self, query):
        """ Function that is used for sync research """
        if query:
            assert type(query) is str, "The search query must be a string.";
            tok_query = query.lower().split(' ');

            ## research making
            # we pick the current time, which takes the initialize time
            t0 = tm.time();

            # starting sync research
            results = [];
            for k, bm25 in enumerate(self.__indexsm):
                result = bm25.get_top_n(tok_query, self.__sources[k], n=3);
                results.extend(result);

            # we pick again the current time, which takes the final time
            t = tm.time();

            ## we return the results found with duration of research.
            return results, round(t - t0, 3);
        else:
            # No query so no result
            return [];

pass;
