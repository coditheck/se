# The Search Engine
Proposition of program of a customized search engine.

<br/>
<br/>

## Installation

### Installation de python3
```
sudo apt install python3
sudo apt install python3-pip
```
Il faut s'assurer de la version de python qui est installée. La version de python
utilisée est `python 3.9.7`. Vous pouvez aussi utiliser version version `3.8`.

<br/>

### Installation de venv
```
sudo apt install python3-venv
```
OU
```
sudo pip3 install virtualenv
```
<br/>

### Créer un environnement virtuel
```
python3 -m venv env
```
OU
```
virtualenv env -p python3
```
<br/>

### Démarrage de l'environnement
```
source env/bin/activate
```
<br/>

### Installation des dépendances
```
pip install -r requirements.txt
```
<br/>
<br/>

## Démarrage du moteur de recherche
```
python3 main.py
```
